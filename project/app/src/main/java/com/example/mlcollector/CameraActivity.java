package com.example.mlcollector;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.common.util.concurrent.ListenableFuture;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;

import static java.lang.Math.abs;


public class CameraActivity extends AppCompatActivity {

    private final Executor executor = Executors.newSingleThreadExecutor();

    private final String POST_PHOTO_URL = "/collector/upload_photo";
    private final int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA",
            "android.permission.WRITE_EXTERNAL_STORAGE"};

    private String projectName;
    private String className;
    private String baseUrl;

    private PreviewView previewView;
    private ImageView overlay;
    private Bitmap overalay_img;
    private Button button;
    private int[] picRes = {0, 0}, picRes2 = {0, 0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        Intent intent = getIntent();
        String overlayPath = intent.getExtras().getString("overlay");
        projectName = intent.getExtras().getString("project");
        className = intent.getExtras().getString("class");
        baseUrl = intent.getExtras().getString("baseUrl");
        Log.d("PATH", String.format("onCreate: overlayPath = %s", overlayPath));
        Log.d("PATH", String.format("onCreate: projectName = %s", projectName));
        Log.d("PATH", String.format("onCreate: className = %s", className));
        Log.d("PATH", String.format("onCreate: baseUrl = %s", baseUrl));

        getSupportActionBar().hide();

        previewView = findViewById(R.id.previewView);
        overlay = findViewById(R.id.overlay);

        // get original bitmap dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(overlayPath, options);
        picRes2[0] = options.outHeight;
        picRes2[1] = options.outWidth;
        Log.d("PICRES", String.format("onCreate: original bitmap hg = %d, wd = %d", picRes2[0], picRes2[1]));

        // get downscaled bitmap from storage
        overalay_img = BitmapFactory.decodeFile(overlayPath);
        picRes[0] = overalay_img.getHeight();
        picRes[1] = overalay_img.getWidth();
        Log.d("PICRES", String.format("onCreate: hg = %d, wd = %d", picRes[0], picRes[1]));

        overlay.setImageBitmap(overalay_img);

        previewView.setScaleType(PreviewView.ScaleType.FIT_CENTER);
        button = findViewById(R.id.camera_capture_button);

        if(allPermissionGranted())
        {
            startCamera();
        }
        else
        {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }


    private void startCamera() {

        // request CameraProvider
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider);
                } catch (ExecutionException | InterruptedException e) {
                    // No errors need to be handled for this Future.
                    // This should never be reached.
                }
        }, ContextCompat.getMainExecutor(this));
    }

    public static int indexOfSmallest(double[] array){
        // add this
        if (array.length == 0)
            return -1;

        int index = 0;
        double min = array[index];

        for (int i = 1; i < array.length; i++){
            if (array[i] <= min){
                min = array[i];
                index = i;
            }
        }
        return index;
    }

    int[] resizeViewHelper(int[] overRes, int[] capRes){
        double [] ratio = {abs(1 - (double)overRes[0]/capRes[0]), abs(1 - (double)overRes[1]/capRes[1]) };

        int lowInd, highInd;
        lowInd = indexOfSmallest(ratio);
        highInd = 1 - lowInd;
        Log.d("RSVIEW", String.format("resizeViewHelper: lowIndex = %d, highIndex = %d", lowInd, highInd));

        int hgNew, wdNew;
        hgNew = capRes[lowInd];
        while((wdNew = overRes[highInd]* hgNew/overRes[lowInd]) > capRes[highInd])
            hgNew--;

        int [] returnList = {0, 0};
        returnList[lowInd] = hgNew;
        returnList[highInd] = wdNew;

        return returnList;
    }

    void resizeView(View view, Size captureSize, int[] prevRes, int[] overRes)
    {
        Log.d("RSVIEW", String.format("resizeView: prevRes.hg = %d, prevRes.wd = %d", prevRes[0], prevRes[1]));
        Log.d("RSVIEW", String.format("resizeView: overRes.hg = %d, overRes.wd = %d", overRes[0], overRes[1]));
        Log.d("RSVIEW", String.format("resizeView: capRes.hg = %d, capRes.wd = %d", captureSize.getWidth(), captureSize.getHeight()));
        int hgNew, wdNew;
        int[] capRes = {captureSize.getWidth(), captureSize.getHeight() };
        int[] newRes = resizeViewHelper(overRes, capRes);

        Log.d("RSVIEW", String.format("resizeView: hgNew = %d, wdNew = %d", newRes[0], newRes[1]));

        wdNew = prevRes[1]*newRes[1]/captureSize.getHeight();
        hgNew = prevRes[1]*newRes[0]/captureSize.getHeight();

        Log.d("RSVIEW", String.format("resizeView: new.hg = %d, new.wd = %d", hgNew, wdNew));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(wdNew, hgNew);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        view.setLayoutParams(params);
    }

    private Bitmap capturedProxy2Bitmap(ImageProxy image) {
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        return BitmapFactory.decodeByteArray(bytes,0,bytes.length,null);
    }



    private void saveAndResizeImage2(ImageProxy image, int newHG, int newWD) {
        Log.d("saveAndResizeImage", String.format("saveAndResizeImage2: imgPlane length=%d, newHG=%d, newWD=%d", image.getPlanes().length, newHG, newWD));
        Bitmap bitmap;
        bitmap = capturedProxy2Bitmap(image);
        Log.d("SAVE", String.format("saveAndResizeImage2: Captured bitmap: hg=%d, wd=%d",
                bitmap.getHeight(), bitmap.getWidth()));

        // here height and width are swapped, because capture bitmap is always landscape
        bitmap = android.media.ThumbnailUtils.extractThumbnail(bitmap, newHG, newWD);
        Log.d("SAVE", String.format("saveAndResizeImage2: Thumbnailed bitmap: hg=%d, wd=%d",
                bitmap.getHeight(), bitmap.getWidth()));

        PostImage(bitmap);
    }

    private void  PostImage(Bitmap image){
        String url = baseUrl + POST_PHOTO_URL + "/" + projectName + "/" + className;
        Log.d("SAVE", url);

        //encode bitmap to base64
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        String encoded = Base64.encodeToString(bytes, Base64.DEFAULT);
//        final ArrayList<NameValuePair> nameValuePairs = new ArrayList<>();
//        nameValuePairs.add(new BasicNameValuePair("file", encoded));
//        Log.d("SAVE", nameValuePairs.toString());

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("file", encoded);
            httpPost.setEntity(new StringEntity(jsonParam.toString()));

            HttpResponse response = httpClient.execute(httpPost);
            Log.d("SAVE", response.toString());

        }
        catch (Exception e) {
            Log.e("SAVE", e.toString());
        }
    }

    @SuppressLint("RestrictedApi")
    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {
        Preview preview = new Preview.Builder()
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                .build();
        ImageCapture.Builder builder = new ImageCapture.Builder();

        final ImageCapture imageCapture = builder
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                .build();

        preview.setSurfaceProvider(previewView.createSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalysis, imageCapture);
        Size previewSize, captureSize;
        previewSize = preview.getAttachedSurfaceResolution();
        captureSize = imageCapture.getAttachedSurfaceResolution();

        int[] prevRes ={previewView.getHeight(), previewView.getWidth()} ;
//        overlay.setImageBitmap(overalay_img);
        resizeView(overlay, captureSize, prevRes, picRes);

        button.setOnClickListener(v -> {

            button.setClickable(false);
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
            File file = new File(getBatchDirectoryName(), mDateFormat.format(new Date())+ ".jpg");
            File bitmapFile = new File(getBatchDirectoryName(), mDateFormat.format(new Date())+ ".png");

            ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(file).build();

            imageCapture.takePicture(executor,new ImageCapture.OnImageCapturedCallback(){
                @Override
                public void onCaptureSuccess(@NonNull ImageProxy image) {
                    saveAndResizeImage2(image, picRes2[0], picRes2[1]);
                    super.onCaptureSuccess(image);
                }

                @Override
                public void onError(@NonNull ImageCaptureException exception) {
                    super.onError(exception);
                }
            });

            button.setClickable(true);
            imageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback () {

                @Override
                public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("File", file.toString());
                            Toast.makeText(CameraActivity.this, "Image Saved successfully", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                @Override
                public void onError(@NonNull ImageCaptureException error) {
                    error.printStackTrace();
                }
            });
        });
    }

    public String getBatchDirectoryName() {
        String app_folder_path = "";
        app_folder_path = getFilesDir().toString() + "/images";
//        app_folder_path = Environment.getExternalStorageDirectory().toString() + "/images";
        File dir = new File(app_folder_path);
        if (!dir.exists() && !dir.mkdirs()) {

        }

        return app_folder_path;
    }

    private boolean allPermissionGranted() {
        for(String permission : REQUIRED_PERMISSIONS){
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }
}