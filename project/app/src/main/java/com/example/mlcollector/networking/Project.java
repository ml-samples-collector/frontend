package com.example.mlcollector.networking;

import com.google.gson.annotations.SerializedName;

public class Project {
    private int id;

    @SerializedName("project_name")
    private String projectName;

    @SerializedName("project_description")
    private String projectDescription;

    public int getId() {
        return id;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }
}
