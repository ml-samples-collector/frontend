package com.example.mlcollector.networking;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIUtils {

    private MLCollectorAPI mlCollectorAPI;
    private Retrofit configRetrofit;

    private List<Project> getProjectsCallResult = null;
    private List<MLClass> getClassesCallResult = null;
    private String csrfToken = null;

    private boolean error = false;
    private String errorMessage = null;

    public APIUtils(String baseUrl) {
        // baseUrl should be: "http://ip:port/collector/"
        configRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mlCollectorAPI = configRetrofit.create(MLCollectorAPI.class);
    }

    public void makeAGetProjectsCall() {
        Call<List<Project>> getProjectsCall = mlCollectorAPI.getProjects();
        getProjectsCall.enqueue(new Callback<List<Project>>() {
            @Override
            public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {
                if (!response.isSuccessful()){
                    error = true;
                    errorMessage = "RESPONSE CODE: " + response.code();
                    return;
                }

                getProjectsCallResult = response.body();
                error = false;
                errorMessage = null;
            }

            @Override
            public void onFailure(Call<List<Project>> call, Throwable t) {
                error = true;
                errorMessage = t.getMessage();
            }
        });
    }

    public void makeAGetClassesCall(String className) {
        Call<List<MLClass>> getClassesCall = mlCollectorAPI.getClasses(className);
        getClassesCall.enqueue(new Callback<List<MLClass>>() {
            @Override
            public void onResponse(Call<List<MLClass>> call, Response<List<MLClass>> response) {
                if (!response.isSuccessful()) {
                    error = true;
                    errorMessage = "RESPONSE CODE:" + response.code();
                    return;
                }

                getClassesCallResult = response.body();
                error = false;
                errorMessage = null;
            }

            @Override
            public void onFailure(Call<List<MLClass>> call, Throwable t) {
                error = true;
                errorMessage = t.getMessage();
            }
        });
    }

    public void callForCsrfToken() {
        Call<ResponseBody> call = mlCollectorAPI.getCsrfToken();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    error = true;
                    errorMessage = "RESPONSE CODE: " + response.code();
                    return;
                }

                error = false;
                errorMessage = null;

                try {
                    String html = response.body().string();
                    Document document = Jsoup.parse(html);
                    Elements elements = document.select("input");

                    for (Element element : elements) {
                        if (element.attr("name").equals("csrfmiddlewaretoken")) {
                            csrfToken = element.attr("value");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                error = true;
                errorMessage = t.getMessage();
            }
        });
    }

    public String getCsrfToken() {
        String tmp = this.csrfToken;
        this.csrfToken = null;

        return tmp;
    }

    public List<Project> getProjects() {
        List<Project> tmpRes = this.getProjectsCallResult;
        this.getProjectsCallResult = null;

        return tmpRes;
    }

    public List<MLClass> getClasses() {
        List<MLClass> tmpRes = this.getClassesCallResult;
        this.getClassesCallResult = null;

        return tmpRes;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
