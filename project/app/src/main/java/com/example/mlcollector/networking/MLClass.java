package com.example.mlcollector.networking;

import com.google.gson.annotations.SerializedName;

public class MLClass {
    private int id;

    @SerializedName("class_name")
    private String className;

    public int getId() {
        return id;
    }

    public String getClassName() {
        return className;
    }
}
