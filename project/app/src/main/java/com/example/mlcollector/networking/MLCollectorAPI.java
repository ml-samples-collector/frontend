package com.example.mlcollector.networking;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MLCollectorAPI {
    /*
    Interface for API calls. It will be implemented automatically by retrofit
     */
    @GET("get_projects")
    Call<List<Project>> getProjects();

    @GET("get_classes/{project_name}")
    Call<List<MLClass>> getClasses(@Path("project_name") String project_name);

    @GET("upload_photo")
    Call<ResponseBody> getCsrfToken();

}
