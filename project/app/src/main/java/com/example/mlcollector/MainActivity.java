package com.example.mlcollector;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private String BASE_URL = "http://192.168.0.30:8000";
    private final String GET_PROJECTS_URL = "/collector/get_projects";
    private final String GET_CLASSES_URL = "/collector/get_classes";
    private final String GET_FRAME_URL = "/collector/get_project_frame";

    Spinner projectSpinner, classSpinner;
    EditText serverUrlEditText;
    String overlaysPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        projectSpinner = findViewById(R.id.projectSpinner);
        classSpinner = findViewById(R.id.classSpinner);
        serverUrlEditText = findViewById(R.id.serverUrlEditText);

        setupServerUrlEditText();
        requestProjects(this);
        setupProjectSpinnerListener(this);
    }

    public void onRefreshButtonClick(View view) {
        projectSpinner.setAdapter(null);
        classSpinner.setAdapter(null);

        requestProjects(this);
    }

    public void onNextButtonClick(View view) {
        Intent intent = new Intent(this, CameraActivity.class);

        String overlay = overlaysPath;
        Log.d("PATH", String.format("onCameraButtonClick: overlayPath = %s", overlay));

        intent.putExtra("overlay", overlay);
        intent.putExtra("project", projectSpinner.getSelectedItem().toString());
        intent.putExtra("class", classSpinner.getSelectedItem().toString());
        intent.putExtra("baseUrl", BASE_URL);
        startActivity(intent);
    }

    private void setupServerUrlEditText(){
        serverUrlEditText.setText(BASE_URL);
        serverUrlEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                BASE_URL = editable.toString();
            }
        });
    }

    private void setupProjectSpinnerListener(MainActivity mainActivity){
        projectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                classSpinner.setAdapter(null);

                String classesUrl = BASE_URL + GET_CLASSES_URL + "/" + parent.getItemAtPosition(position).toString();
                AsyncHttpClient httpClient = new AsyncHttpClient();
                httpClient.get(classesUrl, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        try {
                            List<String> classesList = new ArrayList<>();
                            for (int i = 0; i < response.length(); ++i) {
                                JSONObject rec = response.getJSONObject(i);
                                String name = rec.getString("class_name");
                                classesList.add(name);
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<>
                                    (mainActivity, R.layout.support_simple_spinner_dropdown_item, classesList);
                            classSpinner.setAdapter((adapter));

                        } catch (Exception e) {
                            Log.e("Get classes failed", e.toString());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray response) {
                        Toast.makeText(MainActivity.this, "Request for classes failed. Please restart application", Toast.LENGTH_SHORT).show();
                    }
                });

                AsyncHttpClient httpClient2 = new AsyncHttpClient();
                String frameUrl = BASE_URL + GET_FRAME_URL + "/" + parent.getItemAtPosition(position).toString();
                File file = new File(getFilesDir(), "frame.png");
                httpClient2.get(frameUrl, new FileAsyncHttpResponseHandler(file) {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, File fileFromRequest) {
                        Toast.makeText(MainActivity.this, "Request for frame failed. Please restart application", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, File fileFromRequest) {
                        overlaysPath = fileFromRequest.getAbsolutePath();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void requestProjects(MainActivity mainActivity) {
        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.get(BASE_URL + GET_PROJECTS_URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    List<String> projectsList = new ArrayList<>();
                    for (int i = 0; i < response.length(); ++i) {
                        JSONObject rec = response.getJSONObject(i);
                        String name = rec.getString("project_name");
                        projectsList.add(name);
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>
                            (mainActivity, R.layout.support_simple_spinner_dropdown_item, projectsList);
                    projectSpinner.setAdapter(adapter);

                } catch (Exception e) {
                    Log.e("Get projects failed", e.toString());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray response) {
                Toast.makeText(MainActivity.this, "Request for projects failed. Please restart application", Toast.LENGTH_SHORT).show();
            }
        });
    }
}